#!/usr/bin/env python3

import logging
import os
from xdg import XDG_CACHE_HOME, XDG_CONFIG_HOME, XDG_RUNTIME_DIR

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%Y/%m/%d-%H:%M:%S')
#logging.getLogger("send_sms_freemobile").setLevel(logging.INFO)
#logging.getLogger("urllib3.connectionpool").setLevel(logging.INFO)
logger = logging.getLogger(__name__)


class Application:
    def __init__(self, name):
        self._name = name
        
        runtime_dir = XDG_RUNTIME_DIR or "/tmp"
        self._pid_file = os.path.join(runtime_dir, self._name+'.pid')
        
    def lock(self):
        """Create PID file"""
        if os.path.isfile(self._pid_file):
            raise Exception("Application already launched!")
        
        pid = str(os.getpid())
        with open(self._pid_file, 'w') as pid_file:
            pid_file.write(pid)
        logger.debug(self._pid_file + " locked")
        
    def unlock(self):
        """Unlock PID file"""
        os.unlink(self._pid_file)
        logger.debug(self._pid_file + " unlocked")
    
    
class CheckMyThingsApplication(Application):
    
    def __init__(self):
        super().__init__('check-my-things')
        
    def run(self):
        self.lock()
        self.unlock()
    
    
if __name__ == '__main__':
    main()
